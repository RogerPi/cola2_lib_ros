/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SERVICECLIENTHELPER_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SERVICECLIENTHELPER_H_

#include <cola2_lib/utils/thread_safe_flag.h>
#include <ros/ros.h>
#include <functional>
#include <thread>

namespace cola2
{
namespace ros
{
/**
 * \brief Connect to the specified service name and type.
 *
 * \param nh Node handler that wants to connect to a service.
 * \param srv_name Full service name to connect to.
 * \param wait Time to wait between tries, defaults to 1.0.
 * \tparam ServiceType Type of service used in the connection.
 * \return A ServiceClien to the specified service.
 */
template <class ServiceType>
::ros::ServiceClient connectToService(::ros::NodeHandle& nh, const std::string& srv_name, const double wait = 1.0)
{
  ::ros::ServiceClient service = nh.serviceClient<ServiceType>(srv_name);
  while (::ros::ok())
  {
    if (service.waitForExistence(::ros::Duration(wait)))
    {
      break;
    }
    ROS_WARN("waiting for client to service: %s", srv_name.c_str());
    ::ros::Duration(wait).sleep();
  }
  return service;
}

// Helper function
template <typename TSrv, typename TReq, typename TRes>
void callServiceHelper(TSrv* srv, const TReq* req, TRes* res, bool* success, cola2::utils::ThreadSafeFlag* done)
{
  *success = srv->call(*req, *res);
  done->setState(true);
}

/**
 * \brief This function allows calling a service with a timeout
 * \param[in] srv Service client
 * \param[in] req Service request
 * \param[out] res Service response
 * \param[in] timeout Timeout in seconds. Ihe minimum timeout is 1 second
 * \return It returns true if the service returned on time and false otherwise
 */
template <typename TSrv, typename TReq, typename TRes>
bool callServiceWithTimeout(TSrv& srv, const TReq& req, TRes& res, bool& success, double timeout)
{
  // Start dedicated thread for the service and wait
  timeout = timeout < 0.0 ? 0.0 : timeout;
  cola2::utils::ThreadSafeFlag* done = new cola2::utils::ThreadSafeFlag(false);  // This may need to remain on memory
  std::function<void()> callback_func =
      std::bind(callServiceHelper<TSrv, TReq, TRes>, &srv, &req, &res, &success, done);
  std::thread th(callback_func);
  done->timedBlockingWaitFor(true, timeout);

  // Check if the service returned on time. Join its thread, free memory and return
  if (done->getState())
  {
    th.join();
    delete done;
    return true;
  }

  // Otherwise, detach thread and leak memory. There is nothing better we can do
  th.detach();
  return false;
}
}  // namespace ros
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_SERVICECLIENTHELPER_H_
