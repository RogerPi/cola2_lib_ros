/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_NAV_TRANSFORM_HANDLER_H_
#define COLA2_NAV_TRANSFORM_HANDLER_H_

#include <cola2_lib/utils/angles.h>
#include <cola2_lib_ros/this_node.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_listener.h>
#include <Eigen/Dense>
#include <map>
#include <string>

namespace cola2
{
namespace ros
{
/**
 * \brief Eigen aligned allocator for transforms.
 */
using MapString2Affine3d = std::map<std::string, Eigen::Affine3d, std::less<std::string>,
                                    Eigen::aligned_allocator<std::pair<const std::string, Eigen::Affine3d>>>;

/**
 * \brief The TransformHandler class queries and saves transforms with origin at the vehicle frame.
 */
class TransformHandler
{
private:
  MapString2Affine3d transforms_;           //!< transforms from the robot to the sensors
  tf2_ros::Buffer tf_buffer_;               //!< transform buffer
  tf2_ros::TransformListener tf_listener_;  //!< transform listener
  std::string base_frame_;                  //!< frame used as a base for computing transformations

  /**
   * \brief Convert from ROS TFs to Eigen::Affine3d.
   */
  Eigen::Affine3d tfTransformToEigen(const geometry_msgs::TransformStamped& trans) const;

public:
  /**
   * \brief Constructor that queries the namespace to know vehicle frame.
   */
  TransformHandler();

  /**
   * \brief Constructor that sets the base frame.
   */
  TransformHandler(const std::string& frame);

  /**
   * \brief Get a static transform from the map or query the listener and save.
   */
  bool getTransform(const std::string& frame, Eigen::Affine3d& transform);
  /**
   * \brief Get a transform by querying the listener.
   */
  bool getDynamicTransform(const std::string& frame, Eigen::Affine3d& transform);
  /**
   * \brief Add transform manually (useful when transform listener cannot listen).
   */
  void setTransformManually(const std::string& frame, const double x, const double y, const double z, const double qx,
                            const double qy, const double qz, const double qw);
};
}  // namespace ros
}  // namespace cola2

#endif  // COLA2_NAV_TRANSFORM_HANDLER_H_
