/*
 * Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef THIS_NODE_H
#define THIS_NODE_H

#include <string>

namespace cola2
{
namespace ros
{
/**
 * Returns the namespace of the node without double slashes.
 */
std::string getNamespace();

/**
 * Returns the namespace of the node without double slashes and without the leading dash (useful for frames).
 */
std::string getNamespaceNoInitialDash();

/**
 * Returns the unresolved name of the node (without the namespace).
 * if the node name is /swarm1/girona500/node returns node
 */
std::string getUnresolvedNodeName();

}  // namespace ros
}  // namespace cola2

#endif  // THIS_NODE_H
